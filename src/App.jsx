import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css'
import Home from './components/Routes/Home';
import Menu from "./components/Menu";
import Footer from "./components/Footer";
import Article from "./components/Routes/Article";
import Section from "./components/Routes/Section";

class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <Menu/>
          <div id="main-wrap" className="content-wrapper">
            <div id="main" className="container">
                <Route exact path="/" component={Home} />
                <Route path="/article/:id" component={Article} />
                <Route path="/section/:topicUrl" component={Section} />
            </div>
          </div>
          <Footer/>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;
