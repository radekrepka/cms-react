import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './Menu.css';

export default class Menu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sections: []
        };

        fetch(process.env.REACT_APP_API_URL + "sections/")
            .then(response => response.json())
            .then(data => this.setState({ sections: data.data}));
    }

    render() {
        const { sections } = this.state;

        return (
            <div className="nav header-menu justify-content-center">
                <div className="nav-item">
                    <Link to={"/"} className="nav-link">
                        Home
                    </Link>
                </div>
                {sections.map(topic =>
                    <div className="nav-item">
                        <Link to={"/section/" + topic.url} className="nav-link">
                            {topic.name}
                        </Link>
                    </div>
                )}
            </div>
        );
    }
}
