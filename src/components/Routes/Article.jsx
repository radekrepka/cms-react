import React, { Component } from 'react'

export default class Article extends Component {

    constructor(props) {
        super(props);

        this.state = {
            article: {},
            isLoading: true
        };

        fetch(process.env.REACT_APP_API_URL + "articles/" + props.match.params.id)
            .then(response => response.json())
            .then(data => this.setState({ article: data.data, isLoading: false }));
    }

    render() {
        const { article, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading ...</p>;
        }

        return (
            <section className="content">
                <div className="row">
                    <div className="col-10">
                        <h1>
                            {article.title}
                        </h1>
                        <div className="mt-4">
                            <img src="//d39-a.sdn.szn.cz/d_39/c_img_H_L/yAe8g.jpeg?fl=cro,0,256,1800,1013%7Cres,600,,1%7Cwebp,75" />
                        </div>
                        <div className="mt-1">
                            <p className="text-secondary">
                                Zápas Hertha Berlín-Werder Brémy vysílá Televize Seznam dnes od 18:00 hodin. (Video: DIGI Sport)
                            </p>
                        </div>
                        <div>
                            <p>
                                {article.body}
                            </p>
                        </div>
                    </div>
                    <div className="col-2">

                    </div>
                </div>
            </section>
        )
    }
}
