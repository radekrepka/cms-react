import React, { Component } from 'react';
import { createStore } from 'react-redux';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import 'moment-timezone';
import './Home.css';

export default class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            articles: [],
            frontPage: {},
            isLoading: true
        };

        fetch(process.env.REACT_APP_API_URL + "articles/")
            .then(response => response.json())
            .then(data => this.setState({ articles: data.articles, frontPage: data.frontPage, isLoading: false }));
    }

    render() {
        const { articles, frontPage, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading ...</p>;
        }

        return (
            <div>
                {/*<section className="content-header">*/}
                {/*</section>*/}
                <section className="content">
                    <div className="row frontPage">
                        <div className="col-1">
                            <span className="text-success">
                                <Moment format="HH:mm">
                                    {frontPage.dateAdd}
                                </Moment>
                            </span>
                        </div>
                        <div className="col-8">
                            <Link to={"/article/" + frontPage._id}>
                                <img src="//d39-a.sdn.szn.cz/d_39/c_img_H_L/yAe8g.jpeg?fl=cro,0,256,1800,1013%7Cres,600,,1%7Cwebp,75" />
                            </Link>
                            <div className="mt-5">
                                <Link to={"/article/" + frontPage._id}>
                                    <h1>
                                        {frontPage.title}
                                    </h1>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="mt-5">
                        <div className="row">
                            <div className="col-1"></div>
                            <div className="col-8">
                                <h2>Stalo se</h2>
                                <hr/>
                            </div>
                        </div>
                    </div>
                    <div className="article-list">
                        {articles.map(article =>
                            <div className="row mb-3">
                                <div className="col-1">
                                    <span className="text-success">
                                        <Moment format="HH:mm">
                                            {article.dateAdd}
                                        </Moment>
                                    </span>
                                </div>
                                <div className="col-8">
                                    <div className="row">
                                        <div className="col-3">
                                            <Link to={"/article/" + article._id}>
                                                <img src="//d39-a.sdn.szn.cz/d_39/c_img_G_M/QwIqX.jpeg?fl=cro,0,0,1920,1080%7Cres,600,,1%7Cwebp,75" />
                                            </Link>
                                        </div>
                                        <div className="col-9">
                                            <Link to={"/article/" + article._id}>
                                                <h3>
                                                    {article.title}
                                                </h3>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </section>
            </div>
        )
    }
}
