import React, { Component } from 'react';
import { createStore } from 'react-redux';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import 'moment-timezone';
import './Home.css';

export default class Section extends Component {

    constructor(props) {
        super(props);

        this.state = {
            articles: [],
            isLoading: true
        };

        fetch(process.env.REACT_APP_API_URL + "sections/")
            .then(response => response.json())
            .then(data => this.setState({ articles: data.articles, isLoading: false }));
    }

    render() {
        const { articles, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading ...</p>;
        }

        let articleList;

        if (typeof articles !== 'undefined' && articles.length > 0) {
            articleList = (
                <div className="article-list">
                    {articles.map(article =>
                        <div className="row mb-3">
                            <div className="col-1">
                                        <span className="text-success">
                                            <Moment format="HH:mm">
                                                {article.dateAdd}
                                            </Moment>
                                        </span>
                            </div>
                            <div className="col-8">
                                <div className="row">
                                    <div className="col-3">
                                        <Link to={"/article/" + article._id}>
                                            <img src="//d39-a.sdn.szn.cz/d_39/c_img_G_M/QwIqX.jpeg?fl=cro,0,0,1920,1080%7Cres,600,,1%7Cwebp,75" />
                                        </Link>
                                    </div>
                                    <div className="col-9">
                                        <Link to={"/article/" + article._id}>
                                            <h3>
                                                {article.title}
                                            </h3>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            );
        }
        else {
            articleList = (
                <div>
                    Žádné články.
                </div>
            );
        }

        return (
            <div>
                <section className="content">
                    {articleList}
                </section>
            </div>
        )
    }
}
